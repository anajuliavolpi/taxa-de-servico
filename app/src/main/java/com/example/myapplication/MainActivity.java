package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    EditText valorEditText;
    SeekBar seekBar;
    TextView porcentagemTextView;
    TextView taxaTextView;
    TextView valorFinalTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        valorEditText = findViewById(R.id.valorEditText);

        seekBar = findViewById(R.id.taxaSeekBar);
        porcentagemTextView = findViewById(R.id.porcentagemTextView);

        taxaTextView = findViewById(R.id.taxaTextView);
        valorFinalTextView = findViewById(R.id.valorFinalTextView);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                sincronizaTextView();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    public void sincronizaTextView(){

        NumberFormat percentageFormat = NumberFormat.getPercentInstance();
        int valor = seekBar.getProgress();
        this.porcentagemTextView.setText(percentageFormat.format(valor/100.0));

    }



}
